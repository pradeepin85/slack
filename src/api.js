const axios = require("axios");
const apiUrl = "https://slack.com/api";
var qs = require("qs");

var sf = require("node-salesforce");
var conn = new sf.Connection({
  serverUrl:
    "<your Salesforce server URL (e.g. https://na1.salesforce.com) is here>",
  sessionId: "<your Salesforce session ID is here>",
});

const callAPIMethod = async (method, payload) => {
  console.log(method);

  var conn = new sf.Connection({
    serverUrl:
      "http://na44.stmfa.stm.salesforce.com/services/data/v52.0/connect/cms/content/spaces/0ZuRM00000009Pq0AI/versions",
    sessionId:
      "00DRM000000I2KP!AQoAQKhsluTyuYaZ_juc8dJDh29fGllVv.e4dHFjDdYBztaWpI1xwwqTied_CvE.bwto2fM_0RmVwJov1RVAzyC3JDsNQWTv",
  });

  console.log(conn);

  var records = [];
  conn.query("SELECT Id, Name FROM Account", function (err, result) {
    if (err) {
      return console.error(err);
    }
    console.log("total : " + result.totalSize);
    console.log("fetched : " + result.records.length);
  });

  const modal = {
    type: "modal",
    title: {
      type: "plain_text",
      text: "Create a stickie note",
    },
    submit: {
      type: "plain_text",
      text: "Create",
    },
    blocks: [
      {
        type: "input",
        block_id: "note01",
        label: {
          type: "plain_text",
          text: "Note",
        },
        element: {
          action_id: "content",
          type: "plain_text_input",
          placeholder: {
            type: "plain_text",
            text: "Take a note... ",
          },
          multiline: true,
        },
      },
      {
        type: "input",
        block_id: "note02",
        label: {
          type: "plain_text",
          text: "Color",
        },
        element: {
          type: "static_select",
          action_id: "color",
          options: [
            {
              text: {
                type: "plain_text",
                text: "yellow",
              },
              value: "yellow",
            },
            {
              text: {
                type: "plain_text",
                text: "blue",
              },
              value: "blue",
            },
          ],
        },
      },
    ],
  };

  const args = {
    token: "xoxb-1681461946483-1705153611968-9NL80hjc35MmpEoyyxZk2Qlz",
    trigger_id: payload.trigger_id,
    view: JSON.stringify(modal),
  };

  const result = await axios.post(
    "https://slack.com/api/views.open",
    qs.stringify(args)
  );

  console.log(result);
  return result;

  /*


  function reqListener () {
    console.log(this.responseText);
    const responseJson = JSON.parse(this.responseText);
    const url=record.get("host") + "/services/data/v48.0/tooling/sobjects/ManagedContentType/0T1xx0000004CSOCA2";
    debugger;
    var contentNodes = responseJson.Metadata.managedContentNodeTypes;
        that.state.contentNodes = contentNodes;
        that.setState({ responseJson, responseEndpoint: url});
        
  }

  var oReq = new XMLHttpRequest();
        oReq.addEventListener("load", reqListener);
        oReq.open("GET", record.get("host") + "/services/data/v48.0/tooling/sobjects/ManagedContentType/"+record.get("contentTypeId"));
        oReq.setRequestHeader('Authorization','Bearer '+record.get('sessiontoken'));
        oReq.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
        oReq.setRequestHeader("Access-Control-Allow-Origin", "*");
        oReq.send();

*/

  return {
    type: "modal",
    title: {
      type: "plain_text",
      text: "Gratitude Box",
      emoji: true,
    },
    submit: {
      type: "plain_text",
      text: "Submit",
      emoji: true,
    },
    close: {
      type: "plain_text",
      text: "Cancel",
      emoji: true,
    },
    blocks: [
      {
        type: "input",
        block_id: "my_block",
        element: {
          type: "plain_text_input",
          action_id: "my_action",
        },
        label: {
          type: "plain_text",
          text: "Say something nice!",
          emoji: true,
        },
      },
    ],
  };
};

module.exports = {
  callAPIMethod,
};
